# BattleXYZ
 Game was made in Godot Engine. It's about steering a spacecraft in space and trying to survive between asteroids.

# Controls
|  Key        |  Description |
|-------------|--------------|
| `W` or `Up` | Move up.     |
| `S` or `Down` | Move down. |
| `P`         | Pause.       |
| `X`         | Close game.  |
| `Esc`       | Retry.       |

# Binaries

Executable files are available in [releases page](https://codeberg.org/GamePlayer-PL/BattleXYZ/releases).
