extends Control

#########################################
#  Copyright C 2021 - 2022 GamePlayer   #
#    GNU GENERAL PUBLIC LICENSE 3.0     #
#########################################

func _ready():
	""" Load configuration """
	$pause/v/lastscore.text = "Lastscore: %d"%read_from_disk("lastscore")
	$pause/v/highscore.text = "Highscore: %d"%read_from_disk("highscore")
	$player/player/spacecraft.texture = load("res://textures/spacecraft-"+str(read_from_disk("spacecraft"))+".png")
	$player.animation = "sprite"+str(read_from_disk("spacecraft"))

onready var PLAYTIME = OS.get_system_time_secs()

func _process(_delta):
	""" Manage pause process """
	if $dead/Panel.visible == false and $pause/Panel.visible == false:
		$UI/h/time/time.text = str(OS.get_system_time_secs()-PLAYTIME)
	else:
		PLAYTIME = OS.get_system_time_secs()-int($UI/h/time/time.text)

func save_on_disk(filename2, data):
	""" Save data on disk """
	var file = File.new()
	file.open("user://"+filename2+".var", File.WRITE)
	file.store_var(data)
	file.close()

func read_from_disk(filename2):
	""" Read data from disk """
	var dir = Directory.new()
	if dir.file_exists("user://"+filename2+".var"):
		var file = File.new()
		file.open("user://"+filename2+".var", File.READ)
		var data = file.get_var()
		file.close()
		return data
	return 0

func _exit_tree():
	""" Save scores on exit """
	save_on_disk("lastscore", get_node("player").coins)
	if get_node("player").coins > read_from_disk("highscore"):
		save_on_disk("highscore", get_node("player").coins)


func _input(event):
	""" UI behavior """
	if event.is_action_pressed("ui_exit"):
		_exit_tree()
		get_tree().quit()
	if event.is_action_pressed("ui_cancel"):
# warning-ignore:return_value_discarded
		get_tree().reload_current_scene()

func gameover():
	""" Stop gameplay when player lost his last heart """
	_exit_tree()
	$dead/v/lastscore.text = "Lastscore: "+str($player.coins)
	$dead/v/highscore.text = "Highscore: "+str(read_from_disk("highscore"))
	$dead/AnimationPlayer.play("Visible")
	$player.set_process(false)
	$objectsystem.set_process(false)
	$mouse.texture = load("res://textures/mouse-1.png")

func play_wrench():
	""" Play wrench sound """
	$soundeffects/wrench.play()

func play_rock():
	""" Play rock sound """
	get_node("soundeffects/rock-destroy").play()

func play_coin():
	""" Play coin sound """
	$soundeffects/coin.play()
