extends CPUParticles2D

#########################################
#  Copyright C 2021 - 2022 GamePlayer   #
#    GNU GENERAL PUBLIC LICENSE 3.0     #
#########################################

func _ready():
	""" Setup particles area """
	position = get_viewport_rect().size/2
	emission_rect_extents = get_viewport_rect().size
	
	if not OS.get_name() == "HTML5":
		var obj = load("res://scenes/fog.tscn").instance()
		get_node("../install").add_child(obj)

func _process(_delta):
	""" Process particles area and speed """
	position = get_viewport_rect().size/2
	emission_rect_extents = get_viewport_rect().size
	if get_node("../../../dead/Panel").visible == false and get_node("../../../pause/Panel").visible == false:
		speed_scale = 1+(OS.get_system_time_secs()-get_tree().get_root().get_node("main").PLAYTIME)*0.01
		$CPUParticles2D.visible = true
	else:
		$CPUParticles2D.visible = false
	$CPUParticles2D.position = get_local_mouse_position()
