extends MarginContainer

#########################################
#  Copyright C 2021 - 2022 GamePlayer   #
#    GNU GENERAL PUBLIC LICENSE 3.0     #
#########################################

func _selected(type = "grey"):
	save_on_disk("spacecraft", type)
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/main.tscn")

func save_in_javascript(vara, data):
	JavaScript.eval("window.localStorage.setItem('"+vara+"', '" + data + "')")

func save_on_disk(filename2, data):
	var file = File.new()
	file.open("user://"+filename2+".var", File.WRITE)
	file.store_var(data)
	file.close()

func read_from_disk(filename2):
	var dir = Directory.new()
	if(dir.file_exists("user://"+filename2+".var")):
		var file = File.new()
		file.open("user://"+filename2+".var", File.READ)
		var data = file.get_var()
		file.close()
		return data
	else:
		return 0

func read_from_javascript(vara):
	if JavaScript.eval("window.localStorage.hasOwnProperty('"+vara+"')"):
		return JavaScript.eval("window.localStorage.getItem('"+vara+"')")
	else:
		return 0
