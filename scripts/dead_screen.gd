extends Control

#########################################
#  Copyright C 2021 - 2022 GamePlayer   #
#    GNU GENERAL PUBLIC LICENSE 3.0     #
#########################################
func _ready():
	""" Setup death screen """
	$v.visible = false
	$Panel.visible = false

