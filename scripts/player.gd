extends Node2D

#########################################
#  Copyright C 2021 - 2022 GamePlayer   #
#    GNU GENERAL PUBLIC LICENSE 3.0     #
#########################################

var deg = 0
var play = true
var health = 3
var coins = 0
var animation = "spritegrey"


func _ready():
	""" Setup player scale """
	$player.rotation_degrees = 0
	$player.scale /= 5

var y = 0

func _process(_delta):
	""" Configure position of player spacecraft """
	position.y = get_viewport_rect().size.y/2+y
	position.x = get_viewport_rect().size.x/2-get_viewport_rect().size.x/4
	$player.position.x = 0
	if play == true and !PAUSE:
		game()



func game():
	""" Limitize the polygon where player should be """
	if not(y < -200 and deg == -1 or y > 200 and deg == 1):
		y += deg*get_node("../background/Viewport/CPUParticles2D").speed_scale
		match deg:
			-1:
				if $player.rotation_degrees > -16:
					$player.rotation_degrees -= 1*get_node("../background/Viewport/CPUParticles2D").speed_scale
			1:
				if $player.rotation_degrees < 16:
					$player.rotation_degrees += 1*get_node("../background/Viewport/CPUParticles2D").speed_scale
		return
	deg *= -1

var PAUSE = false

func _input(event):
	""" Manage controlling the player """
	if event.is_action_pressed("back_to_game"):
		PAUSE = not(PAUSE)

	if event.is_action_pressed("ui_up") and !PAUSE:
		deg = -1

	if event.is_action_pressed("ui_down") and !PAUSE:
		deg = 1


func _on_player_body_entered(body):
	""" Define behavior when player will touch something """
	match body.name.left(1):
		"a":
			if not health <= 0:
				health -= 1
				$AnimationPlayer.play(animation)
			match health:
				2:
					get_tree().get_root().get_node("main/UI/3").play("0")
				1:
					get_tree().get_root().get_node("main/UI/2").play("0")
				0:
					get_tree().get_root().get_node("main/UI/1").play("0")
					get_tree().get_root().get_node("main").gameover()
			return
		"c":
			coins += 1
			get_tree().get_root().get_node("main/UI/h/points/Label").text = "%d"%coins
			return
		"d":
			coins += 10
			get_tree().get_root().get_node("main/UI/h/points/Label").text = "%d"%coins
			return
		"w":
			if not health >= 3:
				health += 1
				$AnimationPlayer.play(animation)
				get_tree().get_root().get_node("main").play_wrench()
			match health:
				2:
					get_tree().get_root().get_node("main/UI/2").play("1")
				3:
					get_tree().get_root().get_node("main/UI/3").play("1")
			return
