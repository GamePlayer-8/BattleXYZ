extends AudioStreamPlayer

#########################################
#  Copyright C 2021 - 2022 GamePlayer   #
#    GNU GENERAL PUBLIC LICENSE 3.0     #
#########################################

func _ready():
	""" Play music on startup """
	self.play()
