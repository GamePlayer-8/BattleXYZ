extends Node2D

#########################################
#  Copyright C 2021 - 2022 GamePlayer   #
#    GNU GENERAL PUBLIC LICENSE 3.0     #
#########################################

var PAUSE = false

func mkasteroid():
	""" Create new asteroid """
	var ins = load("res://scenes/asteroid.tscn").instance()
	ins.name = "asteroid:%d"%OS.get_system_time_msecs()
	self.add_child(ins)

var RNG = RandomNumberGenerator.new()
onready var time = OS.get_system_time_msecs()
onready var time2 = OS.get_system_time_msecs()
onready var time3 = OS.get_system_time_msecs()
onready var time4 = OS.get_system_time_msecs()

func _ready():
	""" Randomze seed """
	RNG.randomize()

func _process(_delta):
	""" Spawn new objects """
	if !PAUSE and OS.get_system_time_msecs() - time > round(RNG.randf_range(1000/get_node("../background/Viewport/CPUParticles2D").speed_scale, 5000/get_node("../background/Viewport/CPUParticles2D").speed_scale)):
		mkasteroid()
		time = OS.get_system_time_msecs()
	if !PAUSE and OS.get_system_time_msecs() - time2 > round(RNG.randf_range(10000/get_node("../background/Viewport/CPUParticles2D").speed_scale, 15000/get_node("../background/Viewport/CPUParticles2D").speed_scale)):
		mkcoin()
		time2 = OS.get_system_time_msecs()
	if !PAUSE and OS.get_system_time_msecs() - time3 > round(RNG.randf_range(50000/get_node("../background/Viewport/CPUParticles2D").speed_scale, 60000/get_node("../background/Viewport/CPUParticles2D").speed_scale)):
		mkdcoin()
		time3 = OS.get_system_time_msecs()
	if !PAUSE and OS.get_system_time_msecs() - time4 > round(RNG.randf_range(30000/get_node("../background/Viewport/CPUParticles2D").speed_scale, 45000/get_node("../background/Viewport/CPUParticles2D").speed_scale)):
		mkwrench()
		time4 = OS.get_system_time_msecs()

func mkcoin():
	""" Make coin object """
	var ins = load("res://scenes/coins/coin.tscn").instance()
	ins.name = "coin:%d"%OS.get_system_time_msecs()
	self.add_child(ins)

func mkdcoin():
	""" Make extra coin object """
	var ins = load("res://scenes/coins/diamond_coin.tscn").instance()
	ins.name = "dcoin:%d"%OS.get_system_time_msecs()
	self.add_child(ins)

func mkwrench():
	""" Make wrench object """
	var ins = load("res://scenes/wrench.tscn").instance()
	ins.name = "wrench:%d"%OS.get_system_time_msecs()
	self.add_child(ins)

func _input(event):
	""" Interrupt process on pause """
	if event.is_action_pressed("back_to_game"):
		PAUSE = not(PAUSE)
