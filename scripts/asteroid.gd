extends Node2D

#########################################
#  Copyright C 2021 - 2022 GamePlayer   #
#    GNU GENERAL PUBLIC LICENSE 3.0     #
#########################################

var PAUSE = false
var RNG = RandomNumberGenerator.new()
onready var time = OS.get_system_time_msecs()

func _ready():
	""" Setup position & seed """
	RNG.randomize()
	var random = round(RNG.randf_range(1, 4))
	$Sprite.texture = load("res://textures/asteroids/asteroid-%d"%random+".png")
	position.x = get_viewport_rect().size.x+50
	position.y = RNG.randf_range(0, get_viewport_rect().size.y)
	PAUSE = get_tree().get_root().get_node("main/objectsystem").PAUSE

func _physics_process(_delta):
	""" Movement process """
	if !PAUSE:
		self.linear_velocity.x = -50*get_tree().get_root().get_node_or_null("main/background/Viewport/CPUParticles2D").speed_scale*3
	else:
		self.linear_velocity.x = 0
	if position.x < -20:
		self.queue_free()

	if get_tree().get_root().get_node("main/dead/Panel").visible:
		self.queue_free()

func _on_asteroid_body_entered(body):
	""" Remove self """
	if body.name == "player" or body.name.left(1) == "b":
		get_tree().get_root().get_node("main").play_rock()
		self.queue_free()



func _input(event):
	""" Interrupt on pause """
	if event.is_action_pressed("back_to_game"):
		PAUSE = not(PAUSE)

