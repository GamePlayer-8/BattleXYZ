extends Control

#########################################
#  Copyright C 2021 - 2022 GamePlayer   #
#    GNU GENERAL PUBLIC LICENSE 3.0     #
#########################################

func _ready():
	$v.visible = false
	$Panel.visible = false

func _input(event):
	if(event.is_action_pressed("back_to_game")):
		if($Panel.visible):
			$AnimationPlayer.play("RESET")
			$AnimationPlayer.play("Unvisible")
		else:
			$AnimationPlayer.play("RESET")
			$AnimationPlayer.play("Visible")
