extends Sprite

#########################################
#  Copyright C 2021 - 2022 GamePlayer   #
#    GNU GENERAL PUBLIC LICENSE 3.0     #
#########################################

func _ready():
	""" Hide the black foreground """
	self.visible = true
	$AnimationPlayer.play("Unvisible")

func _process(_delta):
	""" Patch up the size """
	position.x = get_viewport_rect().size.x/2
	position.y = get_viewport_rect().size.y/2
	scale.x = get_viewport_rect().size.x/self.texture.get_size().x
	scale.y = get_viewport_rect().size.y/self.texture.get_size().y

func _on_AnimationPlayer_animation_finished(_anim_name):
	""" Remove black foreground """
	self.queue_free()
