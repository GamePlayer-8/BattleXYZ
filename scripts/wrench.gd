extends RigidBody2D

#########################################
#  Copyright C 2021 - 2022 GamePlayer   #
#    GNU GENERAL PUBLIC LICENSE 3.0     #
#########################################

var PAUSE = false
var RNG = RandomNumberGenerator.new()

func _ready():
	""" Setup wrench object """
	RNG.randomize()
	position.x = get_viewport_rect().size.x+50
	position.y = RNG.randf_range(0, get_viewport_rect().size.y)
	PAUSE = get_tree().get_root().get_node("main/objectsystem").PAUSE

func _process(_delta):
	""" Update position """
	if !PAUSE:
		self.linear_velocity.x = -50*get_tree().get_root().get_node_or_null("main/background/Viewport/CPUParticles2D").speed_scale*3
	else:
		self.linear_velocity.x = 0
	if position.x < -50:
		self.queue_free()

	if get_tree().get_root().get_node("main/dead/Panel").visible:
		self.queue_free()

func _on_wrench_body_entered(body):
	""" Remove self on touching player """
	if body.name.left(1) == "p" or body.name.left(1) == "b":
		self.queue_free()

func _input(event):
	""" Interrupt movement on pause """
	if event.is_action_pressed("back_to_game"):
		PAUSE = not(PAUSE)

